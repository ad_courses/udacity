#importing some useful packages
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import cv2
import helper_functions as help_fun
# %matplotlib inline

#reading in an image
# challange1.jpg
# challange2.jpg
# challange3.jpg
# solidWhiteCurve.jpg
# solidWhiteRight.jpg
# solidYellowCurve.jpg
# solidYellowCurve2.jpg
# solidYellowLeft.jpg
# whiteCarLaneSwitch.jpg
image = mpimg.imread('test_images/challange3.jpg')

grayimg = help_fun.grayscale(image)

# Define a kernel size and apply Gaussian smoothing
kernel_size = 9
blur_gray = help_fun.gaussian_blur(grayimg, kernel_size)

# Define our parameters for Canny and apply
low_threshold = 100
high_threshold = 200
edges = help_fun.canny(blur_gray, low_threshold, high_threshold)

# This time we are defining a four sided polygon to mask
imshape = image.shape
vertices = np.array([[(0,imshape[0]),(450, 330), (490, 300), (imshape[1],imshape[0])]], dtype=np.int32)
masked_edges = help_fun.region_of_interest(edges, vertices)

# Define the Hough transform parameters
# Make a blank the same size as our image to draw on
rho = 5 # distance resolution in pixels of the Hough grid
theta = np.pi/180 # angular resolution in radians of the Hough grid
threshold = 54   # minimum number of votes (intersections in Hough grid cell)
min_line_length = 120 #minimum number of pixels making up a line
max_line_gap = 1000    # maximum gap in pixels between connectable line segments
lines = help_fun.hough_lines(masked_edges, rho, theta, threshold, min_line_length, max_line_gap)

weighted_imgage = help_fun.weighted_img(lines, image, α=0.8, β=1., γ=0.)

plt.imshow(weighted_imgage)  # if you wanted to show a single color channel image called 'gray', for example, call as plt.imshow(gray, cmap='gray')
plt.show()

